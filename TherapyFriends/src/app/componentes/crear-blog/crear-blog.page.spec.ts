import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearBlogPage } from './crear-blog.page';

describe('CrearBlogPage', () => {
  let component: CrearBlogPage;
  let fixture: ComponentFixture<CrearBlogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearBlogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearBlogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
